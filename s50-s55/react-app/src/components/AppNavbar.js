//to use react bootstrap components, you must first import them from the react-bootstrap package
import { Fragment, useContext } from 'react'
import {Container, Navbar, Nav} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'

export default function AppNavbar() {
    const { user } = useContext(UserContext)
    
    return(
        <Navbar bg="light" expand="md" className='sticky-top'>
            <Container fluid>
                <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/">Home</Nav.Link>    
                        <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
                        { (user.id !== null) ?
                        <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                        :
                        <Fragment>
                        <Nav.Link as={Link} to="/login">Login</Nav.Link>
                        <Nav.Link as={Link} to="/register">Register</Nav.Link>
                        </Fragment>
                        }

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
