import {Button, Row, Col} from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function Banner(){
    return (
        <Row>
            <Col className="p-5">
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere, even aliens</p>
                <Link to="/login">
                    <Button variant="primary">Enroll now!</Button>
                </Link>
            </Col>
        </Row>
    )
}
