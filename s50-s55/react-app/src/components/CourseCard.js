import {Col, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import propTypes from 'prop-types';


export default function CourseCard({ course }) {
    const {_id, name, description, price } = course;
    // const [seats, setSeats] = useState(10); 
    // const [count, setCount] = useState(0); 
    // const [isOpen, setIsOpen] = useState(true)
  
    // function enroll(){
    //     setSeats(seats - 1)
    //     setCount(count + 1);
    //   }
    
    // //   use effect has 2 arguments, an arrow function and an array
    // // the array will hold the specific state that you want to observe for. if there is a change in the state inside the array, then the arrow function will run. 
    // // you can leave the array and this will not observe any state
    // useEffect(() => {
    //     if(seats === 0){
    //         setIsOpen(false)
    //     }
    // }, [seats])

    return(
            <Col xs={12} md={4}>
                <Card className='courseCard p-3'>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>

                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>

                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Link className="btn btn-primary" to={`/courses/${_id}/view/`}>View Course</Link>
                    </Card.Body>
                </Card>
            </Col>
    )
}

CourseCard.propTypes = {
    course: propTypes.shape({
        name: propTypes.string.isRequired,
        description: propTypes.string.isRequired,
        price: propTypes.number.isRequired
    })
}