import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights (){
    return(
        <Row className="my-3">
            <Col cs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Home from learn</Card.Title>
                        <Card.Text>
                             Some quick example text to build on the card title and make up thebulk of the card's content.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col cs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Pay now, Study Later</Card.Title>
                        <Card.Text>
                             Some quick example text to build on the card title and make up thebulk of the card's content.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col cs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Be community of our part</Card.Title>
                        <Card.Text>
                             Some quick example text to build on the card title and make up thebulk of the card's content.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}