import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AppNavbar from './components/AppNavbar';
import Register from './pages/Register';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Login from './pages/Login';
import Logout from "./pages/Logout";
import NotFound from "./pages/NotFound";
import CourseView from "./components/CourseView";
import { Container } from 'react-bootstrap';
import { UserProvider } from "./UserContext";

export default function App() {
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    })

    const unsetUser = () => {
        localStorage.clear()
    }
    return (
        <UserProvider value ={{user, setUser, unsetUser}}>
        <Router>
        <AppNavbar/>
            <Container>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/courses" element={<Courses />} />
                    <Route path="/courses/:courseId/view/" element={<CourseView/>}/>
                    <Route path="/register" element={<Register />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/logout" element={<Logout/>}/>
                    <Route path="*" element={<NotFound />} />
                </Routes>
            </Container>
        </Router>
        </UserProvider>
    );
}
