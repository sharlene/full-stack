import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard'
import { Row } from "react-bootstrap";


export default function Courses (){
    const [courses, setCourses] = useState([])
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/`)
        .then(response => response.json())
        .then(result => {
            setCourses(result.map(course => {
                return  (
                    <CourseCard key={course.id} course ={course}/>
                )
            }))
        })
    }, [])
    return (
        <Row className="my-3">
            {courses}
        </Row>
    )
}