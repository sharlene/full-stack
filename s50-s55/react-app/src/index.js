import React from 'react';
import ReactDOM from 'react-dom/client';
import Routes from './App';

//importing react bootstrap package
import 'bootstrap/dist/css/bootstrap.min.css'

// gets the root element from the HTML file and sets it as the root component for the whole application
const root = ReactDOM.createRoot(document.getElementById('root'));

// the 'render' function renders sub-components into the root component
root.render(
  <React.StrictMode>
    <Routes />
  </React.StrictMode>
);
