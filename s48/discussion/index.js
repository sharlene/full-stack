let posts = [];
let count = 1;

// add new post
document.querySelector("#form-add-post").addEventListener('submit', (event) => {
    event.preventDefault();
    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    });

    count++;
    showPosts(posts);
    alert("Post successfully created!");
});

// for showing posts in the div element
const showPosts = (posts) => {
    let post_entries = '';

    posts.forEach((post) => {
        post_entries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>

            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>
        `;
    });

    document.querySelector('#div-post-entries').innerHTML = post_entries; 
};

// Edit post function to transfer the post to edit post
const editPost = (post_id) => {
    let title = document.querySelector(`#post-title-${post_id}`).innerHTML;
    let body = document.querySelector(`#post-body-${post_id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = post_id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
};

// update post
document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault();

    for (let i = 0; i < posts.length; i++) {
        if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;
            showPosts(posts);
            alert("Post successfully updated!");
            break;
        }
    }
});

// Delete post
const deletePost = (post_id) => {
    // Find the index of the post in the array
    const postIndex = posts.findIndex(post => post.id.toString() === post_id);

    if (postIndex !== -1) {
        // Remove the post from the array
        posts.splice(postIndex, 1);
        // Update the displayed posts
        showPosts(posts);
        alert("Post successfully deleted!");
    }
};