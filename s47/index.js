// the 'document' represents the whole HTML page, and the query selector targets a specific element based on its ID, class, or tag name

// id
console.log(document.querySelector("#txt-first-name"))

// class
// document.querySelector(".span-full-name")

// // old methods
// document.getElementById("txt-first-name")
// document.getElementsByClassName("span-full-name")
// document.getElementsByTagName("input")

// Declaration of variables
const txt_first_name = document.querySelector("#txt-first-name");
const txt_last_name = document.querySelector("#txt-last-name");
const txt_age = document.querySelector("#txt-age");
const txt_address = document.querySelector("#txt-address");
let span_full_name = document.querySelector(".span-full-name");
let span_personal_info = document.querySelector(".span-personal-info");

// Function to update the full name span
const updateFullName = () => {
    span_full_name.innerHTML = `${txt_first_name.value} ${txt_last_name.value}`;
};

// Function to update the personal info span
const updatePersonalInfo = () => {
    const fullName = `${txt_first_name.value} ${txt_last_name.value}`;
    const age = txt_age.value;
    const address = txt_address.value;
    span_personal_info.innerHTML = `Hello, I am ${fullName}, ${age} years old. I am from ${address}.`;
};

// Event listener for logging event details
const logEventDetails = (event) => {
    console.log(event.target);
    console.log(event.target.value);
};

// Array of input elements
const inputs = [txt_last_name, txt_first_name, txt_age, txt_address];

// event listener loop
inputs.forEach(input => {
    input.addEventListener('keyup', (event) => {
        updateFullName();
        updatePersonalInfo();
        logEventDetails(event);
    });
});


