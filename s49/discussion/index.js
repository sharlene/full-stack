// get post data
fetch("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.json())
    .then((posts)=> showPosts(posts))


// add new post
document.querySelector("#form-add-post").addEventListener('submit', (event) => {
    event.preventDefault()

    const title = document.querySelector("#txt-title").value
    const body = document.querySelector("#txt-body").value

    // !!! i added the trim function in the udpate post to check if the title or body is empty to simulate the disable attribute for the "create" button when the fields are empty
    if (title.trim() === '' || body.trim() === '') {
        alert("Title or body of the post cannot be empty!")
    } else {
        fetch("https://jsonplaceholder.typicode.com/posts", {
            method: "POST",
            body: JSON.stringify({
                title: title,
                body: body,
                userID: 1 //userID is only used since we're using jsonplaceholder API that requires a userID
            }),
            headers: {"Content-Type": "application/json"}
        })
        .then((response) => response.json())
        .then((result) => {
            console.log(result)
            alert("Post successfully created!")
            // reset the form
            document.querySelector("#form-add-post").reset()
            // close the modal
            $('#addPostModal').modal('hide')
        })
    }
})


// for showing posts in the div element
const showPosts = (posts) => {
    let post_entries = ''

    posts.forEach((post) => {
        post_entries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
    
            <button onclick="editPost('${post.id}')" class="btn btn-primary">Edit</button>
            <button onclick="deletePost('${post.id}')" class="btn btn-danger">Delete</button>
        </div>
        `
    })

    document.querySelector('#div-post-entries').innerHTML = post_entries
}

// transfer the post to edit post
const editPost = (post_id) => {
    let title = document.querySelector(`#post-title-${post_id}`).innerHTML
    let body = document.querySelector(`#post-body-${post_id}`).innerHTML

    document.querySelector("#txt-edit-id").value = post_id
    document.querySelector("#txt-edit-title").value = title
    document.querySelector("#txt-edit-body").value = body

    // show the edit post modal
    $('#editPostModal').modal('show')
}

// update post
document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault()

    const postId = document.querySelector("#txt-edit-id").value // get the post ID
    const title = document.querySelector("#txt-edit-title").value // fetch the updated title
    const body = document.querySelector("#txt-edit-body").value // fetch the updated body

    // !!! i added the trim function in the udpate post to check if the title or body is empty to simulate the disable attribute for the "update" button when the fields are empty
    if (title.trim() === '' || body.trim() === '') {
        alert("Title or body of the post cannot be empty!")
        return
    }

    // fetch the existing post data before the update
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
        .then((response) => response.json())
        .then((existingPost) => {
            // log the current title and body before the update
            console.log("The post has been updated from:")
            console.log(existingPost)

            // perform the update
            fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
                method: "PUT",
                body: JSON.stringify({
                    title: title,
                    body: body,
                    userId: existingPost.userId
                }),
                headers: { "Content-Type": "application/json" }
            })
            .then((response) => response.json())
            .then((result) => {
                console.log("The post has been updated to")
                console.log(result)
                alert("Post successfully updated!")

                // Reset the form
                document.querySelector("#form-edit-post").reset()

                // Close the modal
                $('#editPostModal').modal('hide')
            })
        })
})

// delete post
const deletePost = (post_id) => {
    // fetch the post data before deleting
    fetch(`https://jsonplaceholder.typicode.com/posts/${post_id}`)
        .then((response) => response.json())
        .then((deletedPost) => {
            // log the post to be deleted
            console.log("Deleted Post:", deletedPost)

            // send the delete request
            fetch(`https://jsonplaceholder.typicode.com/posts/${post_id}`, {
                method: "DELETE"
            })
                .then(() => {
                    // remove the post from the displayed posts
                    const postElement = document.querySelector(`#post-${post_id}`);
                    if (postElement) {
                        postElement.remove()
                        alert("Post successfully deleted!")
                    }
                })
        })
}
